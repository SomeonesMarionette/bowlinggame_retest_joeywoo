﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void CreateGame()
        {
            var game = new Game();   
        }

        [Test]
        public void AllGutterGame()
        {
            var game = new Game();
            for (int pins = 0)
            {
                GameRoll() = 10;
                Assert.That(_score = 0);
            }

        }

        [Test]
        public void AllOnesNoSpare()
        {
            var game = new Game();

            for(int pins = 1)
            {
                GameRoll() = 10;
            }

            Assert.That(_score = 10);
        }

        [Test]
        public void OneSpareGame()
        {
            var game = new Game();

            for (int pins = 10)
            {
                GameRoll() = 10;
            }
            //assuming the rest is gutter:
            Assert.That(_score = 16);
        }

        [Test]
        public void OneStrike()
        {
            var game = new Game();

            for (int pins = 10)
            {
                GameRoll = 1;
            }

            Assert.That(_score = 20);
        }

        [Test]
        public void PerfectGame()
        {
            var game = new Game();

            for (int pins = 10)
            {
                GameRoll = 10;
            }

            Assert.That(_score = 300);
        }

        [Test]
        public void RandomGameOne()
        {
            var game = new Game();

            GameRoll(10); GameRoll(10);
            GameRoll(8);GameRoll(2);
            GameRoll(0);GameRoll(0);
            GameRoll(5);GameRoll(5);
            GameRoll(9);GameRoll(1);
            GameRoll(10);GameRoll(10);
            GameRoll(3);GameRoll(7);
            GameRoll(4);GameRoll(6);
            GameRoll(1);GameRoll(8);
            GameRoll(10);GameRoll(10);
            GameRoll(8);GameRoll(0);

            Assert.That(_score = 127);
        }

        [Test]
        public void RandomGameTwo()
        {
            var game = new Game();

            GameRoll(5); GameRoll(5);
            GameRoll(8); GameRoll(2);
            GameRoll(0); GameRoll(0);
            GameRoll(5); GameRoll(5);
            GameRoll(9); GameRoll(1);
            GameRoll(3); GameRoll(7);
            GameRoll(3); GameRoll(7);
            GameRoll(4); GameRoll(6);
            GameRoll(1); GameRoll(8);
            GameRoll(4); GameRoll(4);
            GameRoll(8); GameRoll(0);

            Assert.That(_score = 95);
        }

        [Test]
        public void RandomGameThree()
        {
            var game = new Game();

            GameRoll(5); GameRoll(5);
            GameRoll(7); GameRoll(2);
            GameRoll(3); GameRoll(4);
            GameRoll(5); GameRoll(5);
            GameRoll(9); GameRoll(1);
            GameRoll(3); GameRoll(7);
            GameRoll(10); GameRoll(10);
            GameRoll(10); GameRoll(10);
            GameRoll(10); GameRoll(10);
            GameRoll(4); GameRoll(4);
            GameRoll(8); GameRoll(0);

            Assert.That(_score = 132);
        }
    }
}
